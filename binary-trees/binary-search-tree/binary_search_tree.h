// binary_search_tree.h
#ifndef _BINARY_SEARCH_TREE_H
#define _BINARY_SEARCH_TREE_H

#include "Node.h"

struct Node * insert_node(struct Node *, unsigned int);
//void insert_node(struct Node *, unsigned int);
unsigned int search_node(struct Node *, unsigned int);
struct Node * get_in_order(struct Node *);
struct Node * delete_node(struct Node *, unsigned int);
//void get_node(struct Node **root, unsigned int id);
void print_in_order_traversal(struct Node *);
void print_pre_order_traversal(struct Node *);
void print_post_order_traversal(struct Node *);
struct Node * delete_tree(struct Node *);
#endif
