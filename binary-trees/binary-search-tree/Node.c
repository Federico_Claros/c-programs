// Node.c
#include "Node.h"

struct Node * new_node(unsigned int key)
{
    struct Node *node = NULL;
    node = (struct Node *) malloc(sizeof(struct Node));
    if(NULL == node)
      {
        //error
        return NULL;
      }
    node->key = key;
    node->left = NULL;
    node->right = NULL;
    return node;
}
/*
void display_node(struct Node *node)
{
    if(node == NULL)
      {
        printf("[ERR] this node is empty!");
        return;
      }
    printf("{%p, %u, %p, %p}\n",
            node,
            node->key,
            node->left, node->right);
}
*/
