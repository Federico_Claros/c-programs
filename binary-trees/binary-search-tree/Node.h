// Node.h
#ifndef _NODE_H
#define _NODE_H

#include <stdio.h>
#include <stdlib.h>

struct Node
{
    unsigned int key;
    struct Node *left;
    struct Node *right;
};

struct Node * new_node(unsigned int key);
//void display_node(struct Node *node);

#endif
