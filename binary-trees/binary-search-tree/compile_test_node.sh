#!/bin/bash

gcc -o node.o -c -g Node.c
gcc -o test_node.o -c -g test_node.c
gcc -o run_test_node test_node.o node.o

echo "Test ready: run_test_node"
