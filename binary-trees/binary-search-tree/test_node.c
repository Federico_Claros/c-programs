#include <stdlib.h>

#include "Node.h"

int main()
{
    struct Node *node;
    enum node_state state;

    node = (struct Node *) malloc(sizeof(struct Node));
    state = init_node(&node);
    
    if(state == OK)
      {
        display_node(node);    
      }
    
    return 0;
}
