#include <stdio.h>
#include "binary_search_tree.h"

int main()
{
    struct Node *root;
    
    root = new_node(8);

    root = insert_node(root, 3);
    root = insert_node(root, 1);
    root = insert_node(root, 6);
    root = insert_node(root, 4);
    root = insert_node(root, 7);
    root = insert_node(root, 10);
    root = insert_node(root, 14);
    root = insert_node(root, 13);

    printf("In-order traversal:\n");
    print_in_order_traversal(root);
    printf("Pre-order traversal:\n");
    print_pre_order_traversal(root);
    printf("Post-order traversal:\n");
    print_post_order_traversal(root);

    printf("8 belongs to the tree? (0: false; 1: true)\n");
    printf("%u\n", search_node(root, 8));
    printf("99 belongs to the tree? (0: false; 1: true)\n");
    printf("%u\n", search_node(root, 99));

    printf("Deleting node 1...\n");
    root = delete_node(root, 1);
    printf("Deleting node 14...\n");
    root = delete_node(root, 14);
    printf("Deleting node 6...\n");
    root = delete_node(root, 6);

    printf("Deleting node 8...\n");
    root = delete_node(root, 8);

    printf("Deleting node 99...\n");
    root = delete_node(root, 99);

    printf("In-order traversal, again!\n");
    print_in_order_traversal(root);

    root = delete_tree(root);
}
