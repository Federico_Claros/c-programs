// binary_search_tree.c
#include "binary_search_tree.h"

struct Node * insert_node(struct Node *node, unsigned int key)
{
    if(NULL == node)
      {
        return new_node(key);
      }
    if(node->key > key)
      {
        node->left = insert_node(node->left, key);
        return node;
      }
    if(node->key < key)
      {
        node->right = insert_node(node->right, key);
        return node;
      }
    return node;
}
/*
void insert_node(struct Node *node, unsigned int key)
{
    if(NULL == node)
      {
        printf("Node is null! creating a new one!\n");
        node = new_node(key);
        return;
      }

    if(node->key > key && NULL == node->left)
      {
        printf("Node.left is null! creating a new one on left!\n");
        node->left = new_node(key);
        return;
      }

    if(node->key > key)
      {
        printf("Calling insert_node(left, key)!\n");
        insert_node(node->left, key);
        return;
      }
    
    if(node->key < key && NULL == node->right)
      {
        printf("Node.right is null! creating a new one on right!\n");
        node->right = new_node(key);
        return;
      }

    if(node->key < key)
      {
        printf("Calling insert_node(right, key)!\n");
        insert_node(node->right, key);
        return;
      }
    printf("Exiting function!\n");
}
*/
void print_in_order_traversal(struct Node *root)
{
    if(NULL == root)
        return;

    print_in_order_traversal(root->left);

    printf("{%p, %u, %p, %p}\n", root, root->key, root->left, root->right);
    
    print_in_order_traversal(root->right);
}

void print_pre_order_traversal(struct Node *node)
{
    if(NULL == node)
        return;
    
    printf("{%p, %u, %p, %p}\n", node, node->key, node->left, node->right);
    print_in_order_traversal(node->left);
    print_pre_order_traversal(node->right);
}

void print_post_order_traversal(struct Node *node)
{
    if(NULL == node)
        return;
    
    print_post_order_traversal(node->left);
    print_post_order_traversal(node->right);
    printf("{%p, %u, %p, %p}\n", node, node->key, node->left, node->right);
}

unsigned int search_node(struct Node *node, unsigned int key)
{
    if(NULL == node)
        return 0;    
    if(node->key == key)
        return 1;
    if(node->key > key)
        return search_node(node->left, key);
    if(node->key < key)
        return search_node(node->right, key);
}

struct Node * get_in_order(struct Node *node)
{
    struct Node *aux = NULL;
    
    if (NULL == node)
        return node;

    aux = get_in_order(node->left);

    if(NULL == aux)
        return node;
    
    return aux;
}

struct Node * delete_node(struct Node *node, unsigned int key)
{
    struct Node * aux = NULL;

    if(NULL == node)
        return node;

    if(!search_node(node, key))
      {
        printf("There's no node with key: %u\n", key);
        return node;
      }

    if(node->key > key)
      {
        node->left = delete_node(node->left, key);
        return node;
      }

    if(node->key < key)
      {
        node->right = delete_node(node->right, key);
        return node;
      }

    // check if case 1: no daughters at all.
    if(NULL == node->left && NULL == node->right)
      {
        free(node);
        return NULL;
      }

    // check if case 2: one daughter.
    if(NULL == node->left && NULL != node->right)
      {
        aux = node->right;
        free(node);
        return aux;
      }

    if(NULL != node->left && NULL == node->right)
      {
        aux = node->left;
        free(node);
        return aux;
      }

    //case 3: 2 daughters
    // look for the succesor in-order,
    // copy data to this node and delete succesor in-order
    aux = get_in_order(node->right);
/*
 * Is imposible otherwise it would have droped before
    if(NULL == aux)
      {
        node->key = (node->left)->key;
        free(node->left);
        node->left = NULL;
        return node;
      }
*/
    node->key = aux->key;
    node->right = delete_node(node->right, aux->key);
    return node;
}

struct Node * delete_tree(struct Node *node)
{ 
    if(NULL == node)
      {
        return node;
      }
    printf("deleting node: {%p, %u, %p, %p}\n",
            node, node->key, node->left, node->right);
    node->left = delete_tree(node->left);
    node->right = delete_tree(node->right);
    free(node);
    return NULL;
}
