#!/bin/bash

gcc -o node.o -c -g Node.c
gcc -o bst.o -c -g binary_search_tree.c
gcc -o test_tree.o -c -g test_tree.c
gcc -o test_2 test_tree.o bst.o node.o

echo "The runnable is called: test_2"
