#include <stdio.h>
#include <stdlib.h>
#include "simple_list.h"

enum option {FINISH_PROGRAM, PRINT_ALL_NODES, ADD_NODE, GET_NODE, REMOVE_NODE, UNDEFINED};

#define INT_TO_OPTION(opcion) ( (opcion >= FINISH_PROGRAM && opcion <= REMOVE_NODE) ? (enum option) opcion : UNDEFINED)

int main_menu();

int main(){
    bool exit_program = false;
    struct BaseNode * ptr_base;
    struct ListNode * ptr_node;
    int index;

    init_BaseNode(&ptr_base);

    while(!exit_program){
        switch(main_menu()){
            case FINISH_PROGRAM:
                exit_program = true;
                break;
            case PRINT_ALL_NODES:
                print_list(ptr_base);
                break;
            case ADD_NODE:
                init_ListNode(&ptr_node);
                add_node(&ptr_base,ptr_node);
                ptr_node = NULL;
                break;
            case GET_NODE:
                printf("\nInput an index:\n");
                scanf("%d", &index);
                print_node(get_node(ptr_base, index));
                break;
            case REMOVE_NODE:
                printf("\nInput an index:\n");
                scanf("%d", &index);
                delete_node(&ptr_base, index);
                break;
            default:
                printf("\n[ERR] undefined character input, please try again!\n");
                break;
       }
    }
    return 0;
}

int main_menu(){
    int option;
    printf("1_ Print all nodes.\n");
    printf("2_ Add node.\n");
    printf("3_ Get node.\n");
    printf("4_ Remove node.\n");
    printf("0_ Finish Program.\n");
    scanf("%d", &option);
    return INT_TO_OPTION(option);
}
