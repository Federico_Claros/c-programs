// student_list.h
#include "student.h"

struct ListNode
{
  int index;
  struct Student * ptr_student;
  struct ListNode * ptr_next_node;
};

struct BaseNode
{
  int size;
  struct ListNode * ptr_1st_node;
};

/**
 * Initializes an empty Base node.
 * @param ptr_ptr_base Base node address. 
 * @type struct BaseNode **
 */
extern void init_BaseNode(struct BaseNode **);
/**
 * Initializes an empty List node.
 * @param ptr_ptr_node List node address.
 * @type struct ListNode **
*/
extern void init_ListNode(struct ListNode **);
/**
 * Adds a new node to BaseNode.
 * @param ptr_ptr_base Base node address.
 * @type struct BaseNode **
 * @param ptr_node A pointer to a node, to be added into list.
 * @type struct ListNode *
*/
extern void add_node(struct BaseNode **, struct ListNode *);
/**
 * Print a node into terminal.
 * @param ptr_node
 * @type struct ListNode *
*/
extern void print_node(struct ListNode *);
/**
 * Prints content of the list into terminal.
 * @param ptr_base Base node address.
 * @type struct BaseNode *
*/
extern void print_list(struct BaseNode *);
/**
 * Reorders List's indexes from 0 to N.
 * @param ptr_ptr_first_node First list node address.
 * @type struct ListNode **
*/
extern void reorder_indexes(struct ListNode **);
/**
 * Deletes a list node given an index.
 * @param ptr_ptr_base Base node address.
 * @type struct BaseNode **
 * @param index of the node to be deleted.
 * @type int
*/
extern void delete_node(struct BaseNode **, int);
/**
  * Gets a node given an index number.
  * @param ptr_base Base node address.
  * @type struct BaseNode *
  * @param index
  * @type int
  * @returns A node from the list. Null otherwise.
  * @type struct ListNode *
*/
extern struct ListNode * get_node(struct BaseNode *, int);
