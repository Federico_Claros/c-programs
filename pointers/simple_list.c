#include <stdio.h>
#include <stdlib.h>

#include "simple_list.h"

void init_BaseNode(struct BaseNode ** ptr_ptr_base_node){
  (* ptr_ptr_base_node) = (struct BaseNode *) malloc(sizeof(struct BaseNode));
  
  if(NULL != (* ptr_ptr_base_node)){
    (* ptr_ptr_base_node)->size = 0;
    (* ptr_ptr_base_node)->ptr_1st_node = NULL;
  }
}

void init_ListNode(struct ListNode ** ptr_ptr_node){
    (* ptr_ptr_node) = (struct ListNode *) malloc(sizeof(struct ListNode));

    if(NULL != (* ptr_ptr_node)){
        (* ptr_ptr_node)->index = -1;
        (* ptr_ptr_node)->ptr_next_node = NULL;  
    }
}

void add_node(struct BaseNode ** ptr_ptr_base, struct ListNode * ptr_node){
    struct ListNode * ptr_current;
    bool exit = false;
    
    ptr_node->index = (* ptr_ptr_base)->size;
    
    ptr_current = (* ptr_ptr_base)->ptr_1st_node;
    
    if(NULL == ptr_current){
        (* ptr_ptr_base)->ptr_1st_node = ptr_node;
        (* ptr_ptr_base)->size++;
        return;
    }
    
    while(!exit){
        if(NULL == ptr_current->ptr_next_node){
            ptr_current->ptr_next_node = ptr_node;
            (* ptr_ptr_base)->size++;
            exit = true;
        }
        ptr_current = ptr_current->ptr_next_node;
    }
}

void reorder_indexes(struct ListNode ** ptr_ptr_first_node){
    struct ListNode * ptr_next = (* ptr_ptr_first_node)->ptr_next_node;
    int index = 0;
    
    (* ptr_ptr_first_node)->index = index;
    index ++;
    
    while(NULL != ptr_next){
        ptr_next->index = index;
        ptr_next = ptr_next->ptr_next_node;
        index ++;
    }
}

void delete_node(struct BaseNode ** ptr_ptr_base, int index){
    struct ListNode * ptr_previous;
    struct ListNode * ptr_next;
    struct ListNode * ptr_aux;
    bool exit = false;

    if(NULL == (* ptr_ptr_base)->ptr_1st_node){ return; }

    ptr_previous = (*ptr_ptr_base)->ptr_1st_node;
    ptr_next = ptr_previous->ptr_next_node;
    
    if(ptr_previous->index == index){
        (*ptr_ptr_base)->ptr_1st_node = ptr_next;
        free(ptr_previous);

        reorder_indexes(&((*ptr_ptr_base)->ptr_1st_node));
        (* ptr_ptr_base)->size--;
        return;
    }
    
    while(NULL != ptr_next && !exit){
        if(ptr_next->index == index){
            ptr_aux = ptr_next->ptr_next_node;
            free(ptr_previous->ptr_next_node);
            ptr_previous->ptr_next_node = ptr_aux;
            exit = true;
        }
        ptr_previous = ptr_previous->ptr_next_node;
        ptr_next = ptr_next->ptr_next_node;
    }
    reorder_indexes(&((*ptr_ptr_base)->ptr_1st_node));
    (* ptr_ptr_base)->size--;
}

void print_node(struct ListNode * ptr_node){
  if(ptr_node != NULL){
      printf("\tindex: %d\n", ptr_node->index);
  } else {
      printf("\tNode is NULL!!\n");
  }
}

void print_list(struct BaseNode * ptr_base){
    struct ListNode * ptr_node = ptr_base->ptr_1st_node;
    
    printf("Size: %d\n", ptr_base->size);
    
    while(NULL != ptr_node){
        print_node(ptr_node);
        ptr_node = ptr_node->ptr_next_node;
    }
}

struct ListNode * get_node(struct BaseNode * ptr_base, int index){
  struct ListNode * ptr_node = NULL;
  
  if(index < 0 || ptr_base->size-1 < index){
    return NULL;
  }

  ptr_node = ptr_base->ptr_1st_node;
  
  while(NULL != ptr_node){
    if(ptr_node->index == index){
      return ptr_node;
    }
    ptr_node = ptr_node->ptr_next_node;
  }

  return NULL;
}
