// test_2.c
// brief CRUD on a simple dynamic list using pointers.
//
#include "student_list.h"

enum option
{
    BYE, ADD_STUDENT, SHOW_STUDENTS, SHOW_STUDENT, RMV_STUDENT, UNDEF
};

#define INT_TO_OPTION(opt) ((BYE <= opt && opt <= RMV_STUDENT) ? (enum option) opt : UNDEF)

enum option show_main_menu();
int ask_for_index_between(int min, int max);

int main()
{
    struct BaseNode * ptr_base;
    struct ListNode * ptr_node;
    bool exit_program = false;
    int index;
    
    init_BaseNode(&ptr_base);
    
    while(!exit_program)
      {
        switch(show_main_menu())
          {
            case BYE:
                exit_program = true;
                break;
            case ADD_STUDENT:
                init_ListNode(&ptr_node);
                load_new_student(&ptr_node->ptr_student);
                print_student(ptr_node->ptr_student);
                add_node(&ptr_base, ptr_node);
                ptr_node = NULL;
                break;
            case SHOW_STUDENTS:
                print_list(ptr_base);
                break;
            case SHOW_STUDENT:
                index = ask_for_index_between(0, ptr_base->size);
                
                if(-1 == index)
                  {
                    printf("[ERR] You must put an index between %d and %d.\n",
                           0,ptr_base->size);
                    break;
                  }
                  
                print_student(get_node(ptr_base, index)->ptr_student);
                break;
            case RMV_STUDENT:
                index = ask_for_index_between(0, ptr_base->size);
                
                if(-1 == index)
                  {
                    printf("[ERR] You must put an index between %d and %d.\n",
                           0,ptr_base->size);
                    break;
                  }
                  
                
                delete_node(&ptr_base, index);
                break;
            default:
                printf("[ERR] option not supported!\n");
                break;
        }
    }
    return 0;
}

enum option show_main_menu()
{
    int option;
    printf("\n1_ Enter student.\n");
    printf("2_ Show students.\n");
    printf("3_ Show student.\n");
    printf("4_ Delete student.\n");
    printf("0_ Exit program.\n");
    printf("\nEnter option:");
    scanf("%d", &option);
    return INT_TO_OPTION(option);
}

int ask_for_index_between(int min, int max)
{
    int index;
    printf("\nInput index between [%d to %d)\n", min, max);
    scanf("%d",&index);
    
    if(min > index || index > max) { return -1; }
    return index;
}
