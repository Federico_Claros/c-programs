#include "student.h"

void load_new_student(struct Student ** ptr_student)
{
    
    (*ptr_student) = (struct Student *) malloc(sizeof(struct Student));
    
    if(NULL == (*ptr_student))
      {
        printf("[ERR] Not enough memory!");
        return;
      }
    
    printf("\nEnter student name: ");
    scanf("%s", (*ptr_student)->name);
    printf("\nEnter student age: ");
    scanf("%d", &(*ptr_student)->age);
    printf("\nEnter student sex: ");
    getchar(); (*ptr_student)->sex = getchar();
}

void print_student(struct Student * ptr_student)
{
    if(NULL == ptr_student)
      {
        printf("[ERR] pointer is null!\n");
        return;
      }
    
    printf("{%d, %s, %d, %c}\n", ptr_student->id, ptr_student->name, 
           ptr_student->age, ptr_student->sex);
}
