#include <stdio.h>
#include <stdlib.h>

#include "simple_list.h"


int main(){
    struct BaseNode * ptr_base_node;
    struct ListNode * ptr_node;
    int size = 10;
    int i = 0;
  
    init_BaseNode(&ptr_base_node);
    
    if(&ptr_base_node == NULL){
        printf("[ERR] There's not enough memory! Exiting program...\n");
        return -1;
    }
  
    while(i<size){
        init_ListNode(&ptr_node);
        
        if(&ptr_node == NULL){
            printf("[ERR] There's not enough memory! Exiting program...\n");
            exit(-1);
        }
        
        add_node(&ptr_base_node, ptr_node);
        ptr_node = NULL;
        
        i++;
    }
    
    print_list(ptr_base_node);
    
    delete_node(&ptr_base_node, 2);
    
    print_list(ptr_base_node);
    
    delete_node(&ptr_base_node, 0);
    
    print_list(ptr_base_node);
    
    printf("About to print node 7.\n");
    print_node(get_node(ptr_base_node, 7));
    print_node(get_node(ptr_base_node, 8));

    return 0;
}
