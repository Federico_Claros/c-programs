#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Student {
    int id;
    char name [60];
    int age;
    char sex;
};

typedef enum { false, true } bool;

void set_default_value(struct Student * ptr_student);
void init_students(struct Student * ptr_students, int size);
int main_menu();
void list_all_students(struct Student * ptr_students, int size);
int get_student_id(struct Student * ptr_students, int size);
bool is_empty(struct Student * ptr_student);
void load_new_student(struct Student * ptr_student);

int main(){
    bool finish = false;
    struct Student students [10];
    struct Student * ptr_students = students;
    int size = 10;
    int id;
    
    init_students(ptr_students, size);
    
    while(!finish){
        switch(main_menu()){
            case 1:
                list_all_students(ptr_students, size);
                break;
            case 2:
                id = get_student_id(ptr_students, size);
                if(0 <= id && id <= size){
                    load_new_student(ptr_students+id);
                }
                break;
            case 3:
                id = get_student_id(ptr_students, size);
                if(0 <= id && id <= size){
                    set_default_value(ptr_students+id);
                }
                break;
            default:
                finish = true;
                break;
        }
    }
}

void set_default_value(struct Student * ptr_student){
    strcpy(ptr_student->name, "undefined");
    ptr_student->age = -1;
    ptr_student->sex = '-';
}

void init_students(struct Student * ptr_students, int size){
    int index = 0;
    for(index = 0; index < size; index++){
        (ptr_students+index)->id = index;
        set_default_value(ptr_students+index);
    }
}

int main_menu(){
    int option;
    printf("\n1_ List all students.\n");
    printf("2_ Load new student.\n");
    printf("3_ Delete a student.\n");
    printf("0_ Exit program.\n");
    printf("Enter option: ");
    scanf("%d",&option);
    return option;
}

void list_all_students(struct Student * ptr_students, int size){
    int index = 0;
    printf("{id, name, age, sex}\n");
    for(index = 0; index < size; index++){
        printf("{%d, %s, %d, %c}\n", (ptr_students+index)->id,
            (ptr_students+index)->name, (ptr_students)->age,
            (ptr_students+index)->sex);
    }
}

int get_student_id(struct Student * ptr_students, int size){
    int index;
    list_all_students(ptr_students, size);
    printf("\nEnter student id: ");
    scanf("%d", &index);
    return index;
}

bool is_empty(struct Student * ptr_student){
    return (0 == strcmp(ptr_student->name,"undefined") && -1 == ptr_student->age && '-' == ptr_student->sex)? true : false;
}

void load_new_student(struct Student * ptr_student){
    if(is_empty(ptr_student)){
        printf("\nEnter student name: ");
        scanf("%s",ptr_student->name);
        printf("\nEnter student age: ");
        scanf("%d",&ptr_student->age);
        printf("\nEnter student sex: ");
        getchar();
        ptr_student->sex = getchar();
    }
}
