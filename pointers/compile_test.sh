echo '\t[MSG] init. compiler...'
echo '\t[MSG] Compiling simple_list.c'
gcc -c -g simple_list.c
echo '\t[MSG] Compiling test_simple_list.c'
gcc -c -g test_simple_list.c
echo '\t[MSG] Linking files...'
gcc -o test test_simple_list.o simple_list.o
echo '\t[MSG] Runnable file is called: test'
