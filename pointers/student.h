// student.h

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct Student
{
    int id;
    char name [60]; // fixed arbitrary limitation.
    int age;
    char sex;
};

typedef enum { false, true } bool;

/**
 * Loads a new student in memory. 
 * @param ptr_ptr_student student address to load.
 * @type struct Student **
*/
extern void load_new_student(struct Student **);
/**
 * Prints student on terminal or stdout.
 * @param ptr_student student address to print.
 * @type struct Student *
*/
extern void print_student(struct Student *);
