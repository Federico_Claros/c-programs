//student_list.c

#include "student_list.h"

void init_BaseNode(struct BaseNode ** ptr_ptr_base)
{
    (* ptr_ptr_base) = (struct BaseNode *) malloc(sizeof(struct BaseNode));
  
    if(NULL == (* ptr_ptr_base))
      {
        printf("[ERR] no more memory available!!");
        return;
      }

    (* ptr_ptr_base)->size = 0;
    (* ptr_ptr_base)->ptr_1st_node = NULL;
}

void init_ListNode(struct ListNode ** ptr_ptr_node)
{
    (* ptr_ptr_node) = (struct ListNode *) malloc(sizeof(struct ListNode));

    if(NULL == (*ptr_ptr_node))
      {
        printf("[ERR] no more memory available!!");
        return;
      }
    
    (* ptr_ptr_node)->index = -1;
    (* ptr_ptr_node)->ptr_student = NULL;
    (* ptr_ptr_node)->ptr_next_node = NULL;
}

void add_node(struct BaseNode ** ptr_ptr_base, struct ListNode * ptr_node)
{
    struct ListNode * ptr_current;
    bool exit = false;
    
    if(NULL == (* ptr_ptr_base))
      {
        printf("[ERR] base node argument is null!!");
        return;
      }
    
    ptr_node->index = (* ptr_ptr_base)->size;
    
    ptr_current = (* ptr_ptr_base)->ptr_1st_node;
    
    if(NULL == ptr_current)
      {
        (* ptr_ptr_base)->ptr_1st_node = ptr_node;
        (* ptr_ptr_base)->size++;
        return;
      }
    
    while(!exit)
      {
        if(NULL == ptr_current->ptr_next_node)
          {
            ptr_current->ptr_next_node = ptr_node;
            (* ptr_ptr_base)->size++;
            exit = true;
          }
        ptr_current = ptr_current->ptr_next_node;
      }
}

void reorder_indexes(struct ListNode ** ptr_ptr_first_node)
{
    struct ListNode * ptr_next;
    int index = 0;
    
    if(NULL == (*ptr_ptr_first_node))
      {
        return;
      }
    
    ptr_next = (* ptr_ptr_first_node)->ptr_next_node;
    
    (* ptr_ptr_first_node)->index = index;
    index ++;
    
    while(NULL != ptr_next)
      {
        ptr_next->index = index;
        ptr_next = ptr_next->ptr_next_node;
        index ++;
      }
}

void delete_node(struct BaseNode ** ptr_ptr_base, int index)
{
    struct ListNode * ptr_previous;
    struct ListNode * ptr_next;
    struct ListNode * ptr_aux;
    bool exit = false;

    if(NULL == (*ptr_ptr_base))
      {
        printf("[ERR] base node argument is null!!");
        return;
      }
    
    if(NULL == (* ptr_ptr_base)->ptr_1st_node){ return; }

    ptr_previous = (*ptr_ptr_base)->ptr_1st_node;
    ptr_next = ptr_previous->ptr_next_node;
    
    if(ptr_previous->index == index)
      {
        (*ptr_ptr_base)->ptr_1st_node = ptr_next;
        free(ptr_previous);
        reorder_indexes(&((*ptr_ptr_base)->ptr_1st_node));
        (* ptr_ptr_base)->size--;
        return;
      }
    
    while(NULL != ptr_next && !exit)
      {
        if(ptr_next->index == index)
          {
            ptr_aux = ptr_next->ptr_next_node;
            free(ptr_previous->ptr_next_node);
            ptr_previous->ptr_next_node = ptr_aux;
            exit = true;
          }
        
        ptr_previous = ptr_previous->ptr_next_node;
        ptr_next = ptr_next->ptr_next_node;
     }
    
    reorder_indexes(&((*ptr_ptr_base)->ptr_1st_node));
    (* ptr_ptr_base)->size--;
}

void print_node(struct ListNode * ptr_node)
{
    if (NULL == ptr_node)
      {
        printf("\tNode is null!!\n");
        return;
      }

    printf("\tindex: %d ", ptr_node->index);
    print_student(ptr_node->ptr_student);
}

void print_list(struct BaseNode * ptr_base)
{
    struct ListNode * ptr_node = ptr_base->ptr_1st_node;
    
    printf("Size: %d\n", ptr_base->size);
    printf("\tindex: {id, name, age, sex}\n");
    while(NULL != ptr_node)
      {
        print_node(ptr_node);
        ptr_node = ptr_node->ptr_next_node;
      }
}

struct ListNode * get_node(struct BaseNode * ptr_base, int index)
{
    struct ListNode * ptr_node = NULL;
  
    if(NULL == ptr_base)
      {
        printf("[ERR] base node argument is null!!");
        return NULL;
      }
    
    if(index < 0 || ptr_base->size-1 < index) { return NULL; }

    ptr_node = ptr_base->ptr_1st_node;
  
    while(NULL != ptr_node)
      {
        if(ptr_node->index == index) { return ptr_node; }
        
        ptr_node = ptr_node->ptr_next_node;
      }

    return NULL;
}
